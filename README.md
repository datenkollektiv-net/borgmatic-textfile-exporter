# borgmatic-textfile-exporter

This tool can be used to export borgmatic backup metrics to a prometheus plaintext file.
It can be used as cron job to provide borg metrics to the prometheus node exporter.

```bash
Options:
  -c, --config FILE  Path to config files
  -o, --out PATH     Write path to metrics.prom file
  --help             Show this message and exit.
```

Example execution:

```bash
python metrics.py -c /etc/borgmatic/a.yaml -c /etc/borgmatic/b.yaml -o mymetrics.prom
```

Exported metrics:

* borg_backups_total: Total number of Borg backups
* borg_last_backup_timestamp: Timestamp of the last backup
* borg_unique_size: Uncompressed unique size of the Borg Repo

## Development workflow

Setup:
```bash
pip3 install virtualenv
python3 -m venv new_venv
source ./new_venv/bin/activate
pip3 install -r requirements.txt
```

Run:
```bash
python3 metrics.py
```

Build:
```bash
pyinstaller pyinstaller.spec
```

## Credits

Thanks to Dani Hodovic for providing the [borg-exporter](https://github.com/danihodovic/borg-exporter) service.
This script is basically a part of this service.
